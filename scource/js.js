
    var gSelectedMenuStructure = {
        menuName: "",    // S, M, L
        duongKinhCM: 0,
        suongNuong: 0,
        saladGr: 0,
        drink: 0,
        priceVND: 0
      }

      var gSelectedPizzaType = "" ;

      var gPhanTram = "" ;

      var gResul = "" ;

      var gOrderId = "" ;

      var gDrink = {
        maNuocUong:"" ,
        tenNuocUong: "" ,
        donGia:"",
        ghiChu :"",
        ngayTao : "",
        ngayCapNhat: ""
      } ;

      var gCustomerInfo = {
        menuCombo: "",
        loaiPizza: "",
        hoVaTen: "",
        email: "",
        dienThoai: "",
        diaChi: "",
        loiNhan: "",
        voucher:""
      };


      $(document).ready(function(){
        onPageLoading()

      function onPageLoading () {
        callApiToGetDrinkData()
      }
      // nút gửi 
      $("#btn-gui").on("click" , function(){
       
        gCustomerInfo = getData(gCustomerInfo)

        var vCheck = checkData(gCustomerInfo)

        if(vCheck == true){

          $("#order-user-modal").modal("show")
          
          callApiToGetVoucher()
        }
       
      })
      // nút confirm 
      $("#btn-confirm-user").on("click" , function(){

        $("#order-user-modal").modal("hide")
        $("#order-create-modal").modal("show")
        
      var vObjectRequest = {                                          // tạo 1 biến cuc bộ để lưu tất cả những gì vừa order ban nãy , rồi mới gọi được cho server
        kichCo: gSelectedMenuStructure.menuName,
        duongKinh: gSelectedMenuStructure.duongKinhCM,
        suon: gSelectedMenuStructure.suongNuong,
        salad: gSelectedMenuStructure.saladGr,
        loaiPizza: gCustomerInfo.loaiPizza,
        idVourcher: gCustomerInfo.voucher,
        idLoaiNuocUong: gCustomerInfo.drink,
        soLuongNuoc: gSelectedMenuStructure.drink,
        hoTen: gCustomerInfo.hoVaTen,
        thanhTien: gSelectedMenuStructure.priceVND,
        email: gCustomerInfo.email,
        soDienThoai:gCustomerInfo.dienThoai,
        diaChi: gCustomerInfo.diaChi,
        loiNhan: gCustomerInfo.loiNhan 
      } 
  
        callApiToGetOrderId(vObjectRequest)

      })
      // chọn combo menu và đổi màu 
      $(".btn-small").on("click" , function (){

        var vSelectedPackageStructure = getPackage("S" , 20 , 2 , 200 ,  2 , 150000);
        // gọi method hiển thị thông tin
        vSelectedPackageStructure.displayInConsoleLog();

        changePackageButtonColor("S");

        // gán giá trị của package được chọn vào biến toàn cục để lưu tại đó
        gSelectedMenuStructure = vSelectedPackageStructure;
      })

      // chọn combo menu và đổi màu 
      $(".btn-medium").on("click" , function (){
      
        var vSelectedPackageStructure = getPackage("M" , 25 , 5 , 300 ,  3 ,200000);
        // gọi method hiển thị thông tin
        vSelectedPackageStructure.displayInConsoleLog();

        changePackageButtonColor("M");

        // gán giá trị của package được chọn vào biến toàn cục để lưu tại đó
        gSelectedMenuStructure = vSelectedPackageStructure;
      })

      // chọn combo menu và đổi màu 
      $(".btn-large").on("click" , function (){
        
        var vSelectedPackageStructure = getPackage("L" , 30 , 8 , 500 ,  4 , 250000);
        // gọi method hiển thị thông tin
        vSelectedPackageStructure.displayInConsoleLog();

        changePackageButtonColor("L");

        // gán giá trị của package được chọn vào biến toàn cục để lưu tại đó
        gSelectedMenuStructure = vSelectedPackageStructure;
      })

      // chọn loại pizza 
      $(document).on("click" , ".btn-seafood" , function (){

        gSelectedPizzaType = "OCEAN MANIA";

        console.log("loại pizza : " + gSelectedPizzaType);

        changePackageButtonColorPizzaType(gSelectedPizzaType);
      })

      // chọn loại pizza 
      $(document).on("click" , ".btn-cheese" , function (){

        gSelectedPizzaType = "HAWAIIAN"
      
        console.log("loại pizza : " + gSelectedPizzaType);
        
        changePackageButtonColorPizzaType(gSelectedPizzaType)
      })

      // chọn loại pizza 
      $(document).on("click" , ".btn-bacon" , function (){

        gSelectedPizzaType = "CHEESY CHICKEN BACON"

        console.log("loại pizza : " + gSelectedPizzaType);

        changePackageButtonColorPizzaType(gSelectedPizzaType)
      })

      function getPackage(paramMenu, paramSugarGlass, paraBBQ, paramSalad , paramDrink ,paramPrice) {
        var vSelectedPackageStructure = {  //Đối tượng: gói đăng ký gym được chọn
          menuName: paramMenu, //package = gói đăng ký basic/silver/vip
          duongKinhCM: paramSugarGlass,
          suongNuong: paraBBQ, 
          saladGr : paramSalad ,
          drink : paramDrink ,
          priceVND: paramPrice, //giá 1 tháng
          //method: giá cả năm (phải được tính ra bằng cách nhân với 12, và bớt 15%, (nhân với 0.85))
          
          // method display plan infor - phương thức hiện thi gói đăng ký gym 
          displayInConsoleLog () {
            console.log("%cPACKAGE SELECTED - gói pizza dã được chọn..........", "color:blue");
            console.log(this.menuName);  //this = "đối tượng này" 
            console.log("đường kính : " + this.duongKinhCM + " cm");
            console.log("sườn nướng : " + this.suongNuong);
            console.log("salad : " + this.saladGr + " g");
            console.log("nước ngọt : " + this.drink);
            console.log("giá tiền combo : " + this.priceVND + " VND");
            
          }
        }
        return vSelectedPackageStructure;  //trả lại đối tượng, có đủ dữ liệu (attribute) và các methods (phương thức)
      }

      // đổi màu vùng combopizza
      function changePackageButtonColor(paramMenu){
          var vSmall = $(".btn-small");
          var vMedium = $(".btn-medium");
          var vLarge = $(".btn-large") ;
          
          if(paramMenu === "S"){
            vSmall.removeClass("btn-warning"); 
            vSmall.addClass("btn-success");
            vMedium.addClass("btn-warning");
            vLarge.addClass("btn-warning");
          }
          if(paramMenu === "M"){
            vMedium.removeClass("btn-warning"); 
            vMedium.addClass("btn-success");
            vSmall.addClass("btn-warning");
            vLarge.addClass("btn-warning");
          }
          if(paramMenu === "L"){
            vLarge.removeClass("btn-warning"); 
            vLarge.addClass("btn-success");
            vMedium.addClass("btn-warning");
            vSmall.addClass("btn-warning");
          }
      }

      // đổi màu cho vùng pizzatype
      function changePackageButtonColorPizzaType(paramPizza){

        var vSmall = $(".btn-seafood");
        var vMedium = $(".btn-cheese");
        var vLarge = $(".btn-bacon") ;

        if(paramPizza === "OCEAN MANIA"){
            vSmall.removeClass("btn-warning"); 
            vSmall.addClass("btn-success");
            vMedium.addClass("btn-warning");
            vLarge.addClass("btn-warning");
          }
          if(paramPizza === "HAWAIIAN"){
            vMedium.removeClass("btn-warning"); 
            vMedium.addClass("btn-success");
            vSmall.addClass("btn-warning");
            vLarge.addClass("btn-warning");
          }
          if(paramPizza === "CHEESY CHICKEN BACON"){
            vLarge.removeClass("btn-warning"); 
            vLarge.addClass("btn-success");
            vMedium.addClass("btn-warning");
            vSmall.addClass("btn-warning");
          }
      }

      // gọi API lấy dữ liệu nước uống
      function callApiToGetDrinkData(){
        $.ajax({
          url: "http://42.115.221.44:8080/devcamp-pizza365/drinks" ,
          type: 'GET',
          dataType: 'json', // res là tham số tự do đặt tên gì cũng dc
          success: function(res) {
              loadDataToSelectDrink(res);
              console.log(res)
              gDrink = res
          },
          error: function (ajaxContext) {
              alert(ajaxContext.responseText)
          }
      }); 
      }

      // gọi API lấy voucher 
      function callApiToGetVoucher(){
        $.ajax({
          url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + gCustomerInfo.voucher ,
          type: 'GET',
          dataType: 'json',
          async:false, // res là tham số tự do đặt tên gì cũng dc
          success: function(res) {
              console.log(res)

              gPhanTram = res.phanTramGiamGia ;

              gResul = gSelectedMenuStructure.priceVND - (gPhanTram / 100) *  gSelectedMenuStructure.priceVND
              $(".block-all :input").prop("disabled", true);
              $("#inp-fullname-modal").val(gCustomerInfo.hoVaTen)
              $("#inp-phone-modal").val(gCustomerInfo.dienThoai)
              $("#inp-address-modal").val(gCustomerInfo.diaChi)
              $("#inp-message-modal").val(gCustomerInfo.loiNhan)
              $("#inp-voucher-modal").val(gCustomerInfo.voucher)
              $("#inp-infomation-detail-modal").val(
                "xác nhận : " + gCustomerInfo.hoVaTen + " số điện thoại là :  " + gCustomerInfo.dienThoai + " , địa chỉ :  " + gCustomerInfo.diaChi + "\n" + 
                "menu size : " + gSelectedMenuStructure.menuName +  " , sường nướng : " + gSelectedMenuStructure.suongNuong + " , số lượng nước  " + gSelectedMenuStructure.drink +"\n" +
                "tên nước uống : " + gCustomerInfo.drink  + " \n" +
                "loại pizza : " + gSelectedPizzaType + " giá tiền :  " + gSelectedMenuStructure.priceVND + " , mã giảm giá : " + gCustomerInfo.voucher + "\n" + 
                "phần trăm giảm giá là : " + gPhanTram + "%" + "\n" +
                "số tiền phải trả là : " +  gResul )
          },
          error: function () {
            
              alert("ko thấy phần trăm giảm giá" );
              $(".block-all :input").prop("disabled", true);
              $("#inp-fullname-modal").val(gCustomerInfo.hoVaTen) ;
              $("#inp-phone-modal").val(gCustomerInfo.dienThoai) ;
              $("#inp-address-modal").val(gCustomerInfo.diaChi) ;
              $("#inp-message-modal").val(gCustomerInfo.loiNhan) ;
              $("#inp-voucher-modal").val(gCustomerInfo.voucher) ;
              $("#inp-infomation-detail-modal").val(
                "xác nhận : " + gCustomerInfo.hoVaTen + " số điện thoại là :  " + gCustomerInfo.dienThoai + " , địa chỉ :  " + gCustomerInfo.diaChi + "\n" + 
                "menu size : " + gSelectedMenuStructure.menuName +  " , sường nướng : " + gSelectedMenuStructure.suongNuong + " , số lượng nước  " + gSelectedMenuStructure.drink +"\n" + 
                "tên nước uống : " + gCustomerInfo.drink + " \n" +
                "loại pizza : " + gSelectedPizzaType + " giá tiền :  " + gSelectedMenuStructure.priceVND + " , mã giảm giá : " + gCustomerInfo.voucher + "\n" + 
                "ko có phần trăm giảm giá : 0%" + "\n" +
                "số tiền phải trả là : " +  gSelectedMenuStructure.priceVND )
          }

      });
          
      }

      // gọi API để lấy orderID
      function callApiToGetOrderId(paramObject){
        $.ajax({
          url: "http://42.115.221.44:8080/devcamp-pizza365/orders" ,
          type: 'POST',
          data: JSON.stringify(paramObject),
          contentType: "application/json;charset=UTF-8", // res là tham số tự do đặt tên gì cũng dc
          success: function(res) {
              console.log(res);
              $("#inp-orderid-modal").val(res.orderId);
              alert("Tạo Đơn Hàng Thành Công");
          },
          error: function (ajaxContext) {
              alert(ajaxContext.responseText)
          }
      }); 
      }

      // dỗ dữ liệu nước uống vào thẻ select 
      function loadDataToSelectDrink(paramDrink){
        for (var i = 0 ; i < paramDrink.length ; i++) {
          $('#select-drink').append($('<option>', {
              value: paramDrink[i].maNuocUong,
              text: paramDrink[i].tenNuocUong
          }));
        } 
      }

      // đọc dữ liệu từ input modal
      function getData(pramInfomation){

        pramInfomation.hoVaTen = $("#inp-ten").val().trim();
        pramInfomation.dienThoai = $("#inp-sodienthoai").val().trim();
        pramInfomation.voucher = $("#inp-voucher").val().trim();
        pramInfomation.loiNhan = $("#inp-message").val().trim();
        pramInfomation.diaChi = $("#inp-diachi").val().trim();
        pramInfomation.email = $("#inp-email").val().trim();
        pramInfomation.tenNuocUong = $("#select-drink").find(":selected").text();

        var vCustomerInfo = {
          menuCombo: gSelectedMenuStructure,
          loaiPizza: gSelectedPizzaType,
          hoVaTen: pramInfomation.hoVaTen,
          email: pramInfomation.email,
          dienThoai:  pramInfomation.dienThoai,
          diaChi: pramInfomation.diaChi,
          loiNhan: pramInfomation.loiNhan,
          voucher:  pramInfomation.voucher,
          drink : pramInfomation.tenNuocUong
        }
        return vCustomerInfo;
      }

      function checkData(paramData){
      
        if(paramData.menuCombo.menuName == ""){
          alert("Chọn combo đi bạn ơi");
          return false ;
        }
        if(paramData.loaiPizza == ""){
          alert("Chọn pizza đi bạn ơi");
          return false ;
        }
        if(paramData.drink == "Tất Cả Đồ Uống"){
          alert("chọn đồ uống đi bạn ơi");
          return false;
        }
        if(paramData.hoVaTen == ""){
          alert("Nhập họ và tên") ;
          return false ;
        }
        if(!isEmail(paramData.email)){
          return false ;
        }
        if(isNaN(paramData.dienThoai) || (paramData.dienThoai.length) < 10){ 
          alert("Số điện thoại phải là số và phải có 10 số") ;
          return false ;
        }
        if(paramData.diaChi == ""){
          alert("Nhập địa chỉ") ;
          return false ;
        }
        return true ;
      }
      // hàm check mail đơn giảm
      function isEmail(paramEmail) {
        if (paramEmail < 3) {
          alert("Nhập email...");
          return false;
        }
        if (paramEmail.indexOf("@") === -1) {
          alert("Email phải có ký tự @");
          return false;
        }
        if (paramEmail.startsWith("@") === true) {
          alert("Email không bắt đầu bằng @");
          return false;
        }
        if (paramEmail.endsWith("@") === true) {
          alert("Email kết thúc bằng @");
          return false;
        }
        return true;
      }
  })